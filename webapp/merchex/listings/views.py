from django.shortcuts import render
from django.http import HttpResponse
from listings.models import Band

def hello(request):
    return HttpResponse('<h1>Hello Django!</h1><ul><li><a href="listing/">Listing page</a></li><li><a href="about/">About Us</a></li><li><a href="contact/">Contact Us</a></li></ul>')

def about(request):
    return HttpResponse('<h1>À propos</h1> <p>Nous adorons merch !</p>')

def listing(request):
    bands = "".join([f"<li>{b.id} : {b.name}</li>" for b in Band.objects.all()])
    return HttpResponse(f"""<h1>Listing page</h1>
                        <p>Actual bands :</p>
                        <ul>
                            {bands}
                        </ul>""")

def contact(request):
    return HttpResponse('<h1>Contact us</h1><p>Sorry, i\'m in a cave for now...</p>')